//get form data
//submit via ajax
//get confirmation
//display confirmation
//redirect
  
$(document).ready(function(){
  $("#login-form").on("submit",submitLogin);
  $("#login-form input").focus(function(){
    //remove any error class from form
    $("#login-form .form-group").removeClass("has-error");
  });
});

function submitLogin(e){
  e.preventDefault();
  //let logindata = JSON.stringify(new FormData($("#login-form")));
  let logindata = {"identity":$("#identity").val(),"password":$("#password").val()};
  //show loading message
  $("#login-state").html('<img class="spinner" src="images/spinner.png">');
  
   $.ajax({
    type:"POST",
    url:"ajax/loginhandler.php",
    data:logindata,
    dataType:"json",
    encode:true
  })
  .done(function(data){
    console.log(data);
    $(".spinner").remove();
    //if login is successful
    if(data.success){
      //add has-success class to form groups in login form
      $("#login-form .form-group").addClass("has-success");
      //redirect user to a page
      $("#login-state").html("Hello <strong>"+data.username+"</strong> redirecting...");
      let destination = "index.php";
      setTimeout(function(){
        window.location.href = destination;
      },3000);
    }
    //if login is not successful
    else{
      //add has-error class to form groups in login form
      $("#login-form .form-group").addClass("has-error");
      $("#login-state").text("login unsuccessful");
    }
  });
  
}