$(document).ready(
  function(){
    loadProducts();
  }
);

function loadProducts(){
  var catdata={categories:"0"};
  var sourceurl = "ajax/getproducts.php";
  $.ajax({type:'POST',
          url:sourceurl,
          data:catdata,
          dataType:'json',
          encode:true}
  )
  .done(function(data){
    console.log(data);
    renderProducts(data);
  });
}

function renderProducts(data){
  var total = data.length;
  showTotal(total);
  //get template
  var template = $("#product").html().trim();
  //get container
  var products = $("#products-container");
  //empty the container
  products.empty();
  //get row
  var row = $("#row").html().trim();
  //get data
  for(i=0;i<total;i++){
    var name = data[i].name;
    var image = 'products/'+data[i].image_file;
    var price = data[i].price;
    var desc = data[i].description;
    var id = 'viewdetail.php?id='+data[i].id;
    
    var clone = $(template);
    
    //fill the template
    $(clone).find('.product-title').text(name);
    $(clone).find('.product-image').attr('src',image);
    $(clone).find('.price').text(price);
    $(clone).find('.product-desc').html(desc);
    $(clone).find('.product-detail').attr('href',id);
    
    if(i % 4 == 0){
      var productrow = $(row);
      productrow.append(clone);
    }
    else{
      productrow.append(clone);
    }
    if(i % 4 == 3 || i==(total-1)){
      products.append(productrow);
    }
  }
}

function showTotal(total){
  $(".totalresult").html("Result "+total);
}