<?php
//instantiate navigation class
$navigation = new Navigation();
//fetch navigation items
$navitems = $navigation->getNavigation();

?>
<div class="navbar navbar-default">
  <div class="container-fluid">
  <div class="navbar-header">
    <a href="/" class="navbar-brand">
      Logo
    </a>
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
      <div class="icon-bar"></div>
      <div class="icon-bar"></div>
      <div class="icon-bar"></div>
    </button>
  </div>
 
  <div class="collapse navbar-collapse" id="navbar-menu">
     <form class="navbar-form navbar-right" role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Search">
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <ul class="nav navbar-nav navbar-right" id="main-nav">
      <?php
      //render navigation items
      //render the navigation by looping through array
      if(count($navitems)>0){
        $page = new CurrentPage();
        foreach($navitems as $item){
          $link = $item["page_link"];
          $name = $item["page_name"];
          if($page == $link){
            echo "<li class=\"active\"><a href=\"$link\">$name</a></li>";
          }
          else{
            echo "<li><a href=\"$link\">$name</a></li>";
          }
          
        }
      }
      ?>
    </ul>
  </div>
  </div>
</div>
<!--navigation item template-->
<template id="navitem-template">
  <li class=""></li>
</template>