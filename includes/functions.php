<?php
function generateURL($pagenumber,$selected_cats,$selected_price){
  $baseurl = $_SERVER["PHP_SELF"];
  $baseurl = $baseurl."?"."page=".$pagenumber;
  //categories selected
  $cat_count = count($selected_cats);
  if($cat_count>0){
    foreach($selected_cats as $category){
      $cat_url = "&".urlencode("categories[]")."=".$category;
      $baseurl = $baseurl.$cat_url;
    }
  }
  if($selected_price){
    $baseurl = $baseurl."&"."price=".$selected_price;
  }
  return $baseurl;
}

function checkPasswords($password1,$password2){
  //if passwords meet requirements, return true
  //else return an array of status and error messages
  $result = array();
  $errors = array();
  $len1 = strlen($password1);
  $len2 = strlen($password2);
  //if passwords are not the same
  if($password1 !== $password2){
    array_push($errors,"passwords not the same");
  }
  //if either is less than minimum
  if($len2 < 8 || $len1 < 8){
    array_push($errors,"password too short");
  }
  if($password1 == $password2 && !strpbrk($password1,"1234567890")){
    array_push($errors,"password needs to contain numbers");
  }
  if(count($errors) > 0){
    $result["status"] = false;
    $result["errors"] = implode($errors," and ");
    return $result;
  }
  elseif(count($errors)==0){
    $result["status"] = true;
    return $result;
  }
}
?>