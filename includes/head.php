<?php
print_r($_SESSION);
?>
<head>
    <title>
        <?php
        echo $page_title;
        ?>
    </title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="components/jquery/dist/jquery.min.js"></script>
    <script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</head>