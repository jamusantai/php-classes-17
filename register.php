<?php
include_once("autoloader.php");

?>
<!doctype html>
<html>
  <?php include_once("includes/head.php");?>
  <body>
    <?php include_once("includes/pagenavigation.php"); ?>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <form id="login-form">
            <div class="form-group">
              <label for="email">Email</label>
              <input class="form-control" type="email" id="email" name="email" placeholder="you@domain.com">
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input class="form-control" id="password" type="password" name="password" placeholder="8 characters minimum">
            </div>
            <div class="form-group">
              <label for="email">Username</label>
              <input class="form-control" type="text" id="username" name="username" placeholder="janesmith88">
            </div>
            <div class="center">
            <button class="btn btn-default" type="submit" name="login">Login</button>
            </div>
            <span id="login-state" class="help-block center"></span>
          </form>
        </div>
      </div>
    </div>
    <script src="js/register.js"></script>
  </body>
</html>