<?php
function loadClass($classname){
  $classdir = "classes";
  $classpath = $classdir."/".strtolower($classname).".php";
  if(file_exists($classpath)){
    include_once($classpath);
  }
  elseif(file_exists("../".$classpath)){
    include_once("../".$classpath);
  }
  
}
spl_autoload_register('loadClass');
?>