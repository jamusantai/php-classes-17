<?php
class ShoppingCart{
  private $items = array();
  private $id;
  private $session;
  private $account_id;
  private $db;
  public function __construct(){
    $this->db = new DataStorage();
    $this->session = new SessionManager();
    $this->account_id = $this->session->getVars("account_id");
    //check if shopping cart already exists and if user is logged in
    if(!$this->session->getVars("cart") && $this->account_id){
      //if cart does not exist but user is logged in
      //create a new cart in database
      $query = "INSERT INTO shoppingcart (owner_id,created,status,active)
      VALUES('$this->account_id',NOW(),0,1)";
      if($this->db->query($query)){
        //after shopping cart is created, store its id
        $this->id = $this->db->insert_id;
        $this->session->setVars("cart",array("id" => $this->id));
      }
    }
  }
  public function addItem($item_id){
    //add a product to cart
    $query = "INSERT INTO shoppingcartitems (product_id,added,active) 
    VALUES ('$item_id',NOW(),1)";
    if(!$this->db->runQuery($query)){
      //insert is unsuccessful
    }
    //get item id from insert operation
    else{
      $cartitemid = $this->db->insert_id;
    }
    //add product to the bridging table shopping_items
    $linkquery = "INSERT INTO shopping_items (cart_id,cart_item_id) 
    VALUES ('$this->id','$cartitemid')";
    if(!$this->db->runQuery($linkquery)){
      //insert is unsuccessful
    }
    else{
      return true;
    }
  }
  public function removeItem(){
    
  }
  public function getItems(){
    //get all items belonging in this cart
    $query = "SELECT * FROM shopping_items WHERE cart_id = '$this->id'";
    $result = $this->db->query($query);
    if($result->num_rows > 0){
      while($row = $result->fetch_assoc()){
        array_push($this->items,$row);
      }
      return $this->items;
    }
    else{
      return false;
    }
  }
}
?>