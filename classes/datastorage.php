<?php
class DataStorage{
  private $connection;
  private $result = array();
  private $insertid;
  public $status;
  public $errors = array();
  public function __construct(){
    //initialise connection
    $host=getenv("host");
    $user = getenv("user");
    $password = getenv("password");
    $db = getenv("database");
    if( $this->connection = mysqli_connect($host,$user,$password,$db) ){
      $this->status = true;
    }
    else{
      $this->status = false;
      $this->errors["connection"] = $this->connection->connect_error;
    }
    
  }
  public function runQuery($query){
    //reset array before running query
    //for when two queries are run in a row using same db object
    $this->resetArray();
    //check if query is a select query
    if(strpos(strtolower($query),"select") === 0){
      //if query is a select query
      $query_result = $this->connection->query($query);
      
      if($query_result -> num_rows > 0){
        //if query returns result
        while($row = $query_result -> fetch_assoc()){
          array_push($this -> result, $row);
        }
        return $this->result;
      }
      else{
        return false;
      }
    }
    else{
      //if query is not a select query
      if($this->connection->query($query)){
        $this->insertid = $this->connection->insert_id;
        return true;
      }
      else{
        return false;
      }
    }
  }
  public function getInsertId(){
    return $this->insertid;
  }
  public function getConnection(){
    //return connection object
    return $this->connection;
  }
  private function resetArray(){
    $this->result = array();
  }
}
?>