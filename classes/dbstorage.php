<?php
class DBStorage{
  private $errors = array();
  public $status;
  private $connection;
  public function __construct(){
    //initialise connection
    $host=getenv("host");
    $user = getenv("user");
    $password = getenv("password");
    $db = getenv("database");
    if( $this->connection = mysqli_connect($host,$user,$password,$db) ){
      $this->status = true;
    }
    else{
      $this->status = false;
      $this->errors["connection"] = $this->connection->connect_error;
    }
  }
  //bind takes a statement and an associative array of parameters and types
  //statement is the sql statement
  //params is an array containing arrays of values to 
  //types is an array of types
  public function insert($statement,$params,$types){
    $preparation = $this->connection->prepare($statement);
    $preparation->bind_param(implode("",$types), $firstname, $lastname, $email);
  }
  private function checkDataType($params){
    $types = array();
    //check what kind of variable
    foreach($params as $value){
      switch(gettype($value)){
        case "integer":
          array_push($types,"i");
          break;
        case "boolean":
          array_push($types,"i");
          break;
        case "double":
          array_push($types,"d");
          break;
        case "string":
          array_push($types,"s");
          break;
        default:
          array_push($types,"s");
          break;
      }
    }
  }
  private function getColumnNames($statement){
    
  }
}
?>