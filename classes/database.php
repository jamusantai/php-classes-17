<?php
class Database{
  private $connection;
  //array of result
  private $data = array();
  //query string
  private $query;
  
  public function __construct($query=NULL){
    $conn = new DatabaseConnection();
    $this->connection = $conn->getConnection();
    $this->query = $query;
    $this->runQuery();
  }
  private function runQuery(){
    $result = $this->connection->query($this->query);
    if($result->num_rows > 0){
      while($row = $result->fetch_assoc()){
        array_push($this->data,$row);
      }
    }
  }
  public function Data(){
    return $this->data;
  }
  public function JSON(){
    return json_encode($this->data);
  }
}
?>