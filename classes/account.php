<?php
class Account{
  private $database;
  private $session;
  private $id;
  private $errors;
  public function __construct(){
    $this->database = new DataStorage();
    $this->session = new SessionManager();
  }
  public function create($email,$password,$username=NULL){
    //create a new account (register)
    //validate email
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
      $this->errors["email_validity"] = "not a valid email";
      return false;
    }
    if(!$this->uniqueEmail($email)){
      $this->errors["email_duplicate"] = "email already used";
      return false;
    }
    //if username is specified
    if($username!==NULL && !$this->uniqueUser($username)){
      $this->errors["username"] = "username already used";
      return false;
    }
    //sanitize email and username
    $email = filter_var($email,FILTER_SANITIZE_EMAIL);
    $username = filter_var($username,FILTER_SANITIZE_STRING);
    $password = password_hash($password,PASSWORD_DEFAULT);
    //construct query
    $defaultprofileimage = "profile.png";
    $query = "INSERT INTO accounts 
    (email,username,password,created,updated,lastlogin,active,profile_image) 
    VALUES ('$email','$username','$password',NOW(),NOW(),NOW(),1,'$defaultprofileimage')";
    //run query
    if(!$this->database->runQuery($query)){
      $this->errors["account_create"] = "error creating account";
    }
    else{
      //user creation is successful
      //get user id
      $userid = $this->database->getInsertId();
      $this->createUserSession($username,$email,$userid);
    }
  }
  //authenticate user
  public function authenticate($emailoruser,$password){
    //check if email or username is used
    //sanitize email or username
    $emailoruser = filter_var($emailoruser,FILTER_SANITIZE_EMAIL);
    //validate email
    if(filter_var($emailoruser,FILTER_VALIDATE_EMAIL)){
      $email = true;
    }
    else{
      $email = false;
    }
    
    //construct query based on whether an email address or username
    //is supplied by the user
    $query = "SELECT * FROM accounts ";
    if($email===true){
      $query = $query." "."WHERE email='$emailoruser' AND active=1";
    }
    else{
      $query = $query." "."WHERE username='$emailoruser' AND active=1";
    }
    //run the query
    $result = $this->database->runQuery($query);
    //if there is a result (expected 1)
    if(count($result) == 0){
      return false;
      $this->errors["account_exist"] = "account not found or deactivated";
    }
    else{
      $id = $result[0]["account_id"];
      $email = $result[0]["email"];
      $username = $result[0]["username"];
      $hashed = $result[0]["password"];
      //check if passwords match
      //if passwords don't match
      if(!password_verify($password,$hashed)){
        $this->errors["account_credential"] = "credentials don't match";
        return false;
      }
      else{
        //create session variables
        // $this->session->regenerate();
        $this->session->setVars(array("account_id"=>$id,"email"=>$email,"username"=>$username));
        //update login time in database
        $update_query = "UPDATE accounts SET lastlogin=NOW() WHERE email='$email'";
       
        if($this->database->runQuery($update_query)){
          return true;
        }
      }
    }
  }
  public function logOut(){
    //unset user session variables
    $this->session->deleteVars("account_id");
    $this->session->regenerate();
  }
  private function uniqueEmail($email){
    //check if an email address is unique, eg no duplicates in database
    $query = "SELECT email FROM accounts WHERE email='$email'";
    $result = $this->database->runQuery($query);
    if(count($result) > 0){
      //if there is a result, then email is not unique
      return false;
    }
    else{
      return true;
    }
  }
  private function uniqueUser($username){
    //check if a username is unique
    $query = "SELECT username FROM accounts WHERE username='$username'";
    $result = $this->database->runQuery($query);
    if(count($result) > 0){
      return false;
    }
    else{
      return true;
    }
  }
  //this function creates user session variables
  private function createUserSession($user,$email,$id){
    $vars = array();
    if($user){
      $vars["username"] = $user;
    }
    if($email){
      $vars["email"] = $email;
    }
    if($id){
      $vars["account_id"] = $id;
    }
    $this->session->setVars($vars);
  }
  public function getErrors(){
    return $this->errors;
  }
}
?>