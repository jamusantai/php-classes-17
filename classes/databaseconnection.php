<?php
class DatabaseConnection{
  private $connection;
  private $user;
  private $host;
  private $password;
  private $database;
  public function __construct(){
    $this->user = getenv('user');
    $this->password = getenv('password');
    $this->host  = getenv('host');
    $this->database = getenv('database');
    if($this->connect()){
      $this->connection = $this->connect();
      $this->getConnection();
    }
  }
  private function connect(){
    return mysqli_connect(
      $this->host,
      $this->user,
      $this->password,
      $this->database
      );
  }
  public function getConnection(){
    return $this->connection;
  }
}
?>