<?php
class Products{
  //query
  private $query = "SELECT 
  products.id AS id,
  products.name AS name,
  products.description AS description,
  products.price AS price,
  products_categories.category_id AS category_id,
  images.image_file AS image_file
  FROM products
  INNER JOIN products_categories 
  ON products.id = products_categories.product_id 
  LEFT JOIN products_images
  ON products.id = products_images.product_id 
  INNER JOIN images 
  ON products_images.image_id = images.image_id";
  //category
  private $categories = array();
  //price
  private $price;
  //products array
  private $products = array();
  
  public function __construct($categories=NULL,$price=NULL,$page=NULL){
    $this->price = $price;
    if($categories[0]==0){
      $this->categories=NULL;
    }
    else{
      $this->categories = $categories;
    }
    $this->getProducts();
  }
  
  private function getProducts(){
    if(count($this->categories) > 0 && $this->categories !== NULL){
      //see how many categories are requested
      $this->query = $this->query." "."WHERE";
      $cat_total = count($this->categories);
      foreach($this->categories as $category){
        $index++;
        $this->query = $this->query." "."products_categories.category_id=$category";
        if($index < $cat_total){
          $this->query = $this->query." "."OR";
        }
      }
      // "WHERE products_categories.category_id='$this->category'";
      $this->query = $this->query." "."GROUP BY products.id";
      //$result = new Database($this->query);
      //$this->products = $result->Data();
      $this->products = new DataStorage($this->query);
    }
    else{
      $this->query = $this->query." "."GROUP BY products.id";
      echo $this->query;
      //$result = new Database($this->query);
      //$this->products = $result->Data();
      $this->products = new DataStorage($this->query);
    }
  }
  //renderProducts renders the products in bootstrap columns
  public function renderProducts($column=3){
    $totalproducts = count($this->products);
    $items_row = 12/$column;
    $last_item = $totalproducts - ($totalproducts % $items_row);
    foreach($this->products as $product_item){
      $id = $product_item["id"];
      $name = $product_item["name"];
      $text = new WordCount($product_item["description"]);
      $description = $text->truncate(5);
      $price = $product_item["price"];
      $image = $product_item["image_file"];
      $category = $product_item["category_name"];
      $category_id = $product_item["category_id"];
      $counter++;
      $total++;
      if($counter==1){
        echo "<div class=\"row\">";
      }
      echo "<div class=\"product\ col-md-$column\">
      <h4 class=\"product-name\">$name</h4>
      <img src=\"products/$image\" class=\"img-responsive\">
      <h5 class=\"price\">$price</h5>
      <p>$description</p>
      </div>";
      if($counter==$items_row || $total==$last_item){
        echo "</div>";
        $counter=0;
      }
    }
  }
  public function getMaxPrice(){
    $maxquery = "SELECT MAX(price) FROM products";
    $result = new Database($maxquery);
    $pricearray = $result->Data();
    $price = $pricearray[0]["MAX(price)"];
    return $price;
  }
  public function renderJSON(){
    $result = array();
    foreach($this->products as $product){
      $id = $product["id"];
      $name = $product["name"];
      $text = new WordCount($product["description"]);
      $description = $text->truncate(10);
      $image = $product["image_file"];
      $price = $product["price"];
      $product_array = array("id"=>$id,"name"=>$name,"description"=>$description,"image_file"=>$image,"price"=>$price);
      array_push($result,$product_array);
    }
    echo json_encode($result);
  }
  //this function returns a single product
  static function getProduct($id,$json=false){
    $id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
    $productquery = "SELECT * FROM products WHERE products.id='$id'";
    $result = new Database($productquery);
    $product = $result->Data();
    $imagequery = "SELECT images.image_file FROM images INNER JOIN 
    products_images ON images.image_id=products_images.image_id 
    WHERE products_images.product_id='$id'";
    $imageresult = new Database($imagequery);
    $images = $imageresult->Data();
    if($json){
      return json_encode(array("product"=>$product,"images"=>$images));
    }
    else{
      return array("product"=>$product,"images"=>$images);
    }
  }
  public function getQuery(){
    return $this->query;
  }
}
?>