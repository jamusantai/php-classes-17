<?php
class ImageUploader{
  private $upload_dir = "uploads";
  private $file;
  private $database;
  private $session;
  public function __construct($dir=null){
    if(isset($dir)){
      $this->upload_dir = $dir;
    }
    $this->database = new DataStorage();
    $this->session = new SessionManager();
  }
  public function upload($file){
    $this->file = $file;
  }
}
?>