<?php
//perform checks for passwords
/*this class gives a score for a password based on
- 1 score for length of password, has to be 8 or more characters
- 1 score for whether the password contains at least one number
- 1 score for whether the password contains at least one uppercase letter
- 1 score for whether the password contains symbols
A password needs to obtain a score of at least 3 to pass
*/
class PasswordCheck{
  private $password;
  private $pwminlength = 8;
  private $minscore = 3; //max is 4
  private $score = 0;
  private $status;
  public function __construct($password){
    $this->password = $password;
    $this->check();
  }
  private function check(){
    $pass = $this->password;
    $errors = array();
    //check length
    if(strlen($pass) < $this->pwminlength){
      $errors["length"] = "minimum ".$this->pwminlength." characters";
    }
    else{
      //award a point for password =/longer than minimum
      $this->score++;
    }
    
    //check complexity
    $pwarray = str_split($pass);
    $charcount = strlen($pass);
    $numbers = 0;
    $uppers = 0;
    $symbols = 0;
    
    //loop through all password characters and check for 
    //numbers, uppercase and non alpha numeric characters
    foreach($pwarray as $char){
      if(ctype_digit($char)){
        $numbers++;
      }
      if(ctype_upper($char)){
        $uppers++;
      }
      if(ctype_punct($char)){
        $symbols++;
      }
    }
    //add some error messages
    if($numbers == 0){
      $errors["numbers"] = "need to contain a number";
    }
    if($uppers == 0){
      $errors["uppercase"] = "need to contain an uppercase character";
    }
    if($symbols == 0){
      $errors["symbols"] = "need to contain symbols";
    }
    //check if password is all numbers
    if($numbers == $charcount){
      //password is all numbers
      $this->score--;
      $this->status = false;
      return false;
      exit();
    }
    if($uppers == $charcount){
      //password is all uppers
      $this->score--;
      $this->status = true;
      return false;
      exit();
    }
    if($symbols == $charcount){
      //password is all symbols
      $this->score--;
      $this->status = false;
      return false;
      exit();
    }
    //award a score for numbers
    if($numbers > 0){
      $this->score++;
    }
    //award a score for uppers
    if($uppers > 0){
      $this->score ++;
    }
    //award a score for symbols
    if($symbols > 0){
      $this->score++;
    }
    // minimum score is 3
    if($this->score >= $this->minscore){
      $this->status = true;
      return true;
    }
    else{
      $this->status = false;
      return false;
    }
    //end of function
  }
  public function getScore(){
    return $this->score;
  }
  public function __toString(){
    return (string)$this->score;
  }
}
?>