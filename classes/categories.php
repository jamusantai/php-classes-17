<?php
class Categories{
  private $categories = array();
  private $query;

  public function __construct(){
    $this->query = "SELECT category_id,category_name FROM categories WHERE active=1";
    $data = new Database($this->query);
    $this->categories = $data->Data();
  }
  
  public function render(){
    foreach($this->categories as $category){
      $catid = $category["category_id"];
      $catname = $category["category_name"];
    echo "<div class=\"checkbox\">
      <label>
        <input type=\"checkbox\" value=\"$catid\" name=\"categories\">
        $catname
      </label>
    </div>";
    }
  }
  public function getJSON(){
    return json_encode($this->categories);
  }
}
?>