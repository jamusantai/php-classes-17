<?php
class CurrentPage{
  public $url;
  public function __construct(){
    $this->url = $_SERVER['REQUEST_SCHEME'].
    "://".
    $_SERVER['HTTP_HOST'].
    $_SERVER['REQUEST_URI'];
    return basename($this->url);
  }
  public function __toString(){
    return basename($this->url);
  }
  public function Full(){
    return $this->url;
  }
}
?>