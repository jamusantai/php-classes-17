<?php
class Products2{
  private $db;
  private $results = array();
  private $categories = array();
  private $query = "SELECT 
  products.id AS id,
  products.name AS name,
  products.description AS description,
  products.price AS price,
  products_categories.category_id AS category_id,
  images.image_file AS image_file
  FROM products
  INNER JOIN products_categories 
  ON products.id = products_categories.product_id 
  LEFT JOIN products_images
  ON products.id = products_images.product_id 
  INNER JOIN images 
  ON products_images.image_id = images.image_id";
  
  public function __construct($categories=NULL,$price=NULL,$json=false){
    $this->categories = $categories;
    $this->price = $price;
    $this->json = $json;
    $this->db = new DataStorage();
    $this->getProducts();
  }
  
  private function getProducts(){
    if($this->categories == NULL && $this->price == NULL){
      $this->results = $this->db->runQuery($this->query);
      $this->returnResult();
    }
    if(count($this->categories) > 0){
      //add categories as a WHERE clause is products
      $counter = 0;
      $this->query = $this->query." "."WHERE";
      foreach($this->categories as $category){
        $counter++;
        $this->query = $this->query." "."WHERE";
      }
    }
  }
  
  private function returnResult(){
    $this->truncateDescription();
    //columns = id,name,description,price,category_id,image_file
    if($this->json){
      echo json_encode($this->results);
    }
    else{
      return $this->results;
    }
  }
  
  private function truncateDescription(){
    $len = count($this->results);
    $i=0;
    for($i=0;$i<$len;$i++){
      $this->results[$i]["description"] = $this->truncate($this->results[$i]["description"]);
    }
  }
  
  private function truncate($str){
    $text = new WordCount($str);
    return $text->truncate(15);
  }
  
  //static method to used to get one product and images
  static function single($id,$json=false){
    //sanitize id
    $id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
    //product query
    $productquery = "SELECT * FROM products WHERE products.id='$id'";
    $productdb = new DataStorage();
    $product = $productdb->runQuery($productquery);
    //image query
    $imagequery = "SELECT images.image_file FROM images INNER JOIN 
    products_images ON images.image_id=products_images.image_id 
    WHERE products_images.product_id='$id'";
    $imagedb = new DataStorage();
    $images = $imagedb->runQuery($imagequery);
    if($json){
      return json_encode(array("product"=>$product,"images"=>$images));
    }
    else{
      return array("product"=>$product,"images"=>$images);
    }
    
  }
}
?>