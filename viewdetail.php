<?php
include("autoloader.php");
if($_SERVER["REQUEST_METHOD"]=="GET" && $_GET["id"]){
  $id = filter_var($_GET["id"],FILTER_SANITIZE_NUMBER_INT);
  $details = Products2::single($id,false);
  $productid = $details["product"][0]["id"];
  $productname = $details["product"][0]["name"];
  $productprice = $details["product"][0]["price"];
  $description = $details["product"][0]["description"];
}
?>
<!doctype html>
<html>
  <?php include("includes/head.php");?>
  <body>
    <?php include_once("includes/pagenavigation.php"); ?>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="row">
            <div class="col-md-12">
              <h2 class="product-detail-name"><?php echo $productname ?></h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8 col-sm-6">
              <?php
                $counter = 0;
                foreach($details["images"] as $image){
                  $imagefile = $image["image_file"];
                  $counter++;
                  echo "<img src=\"products/$imagefile\" 
                  class=\"img-responsive\" 
                  data-image-count=\"$counter\">";
                }
              ?>
            </div>
            <div class="col-md-4 col-sm-6">
              <?php
              echo "<p>$description</p>";
              echo "<p class=\"price\">$productprice</p>";
              ?>
              <button class="btn btn-info" data-id="<?php echo $productid;?>">
                Add to Cart
                <span class="glyphicon glyphicon-shopping-cart"></span>
              </button>
              <button class="btn btn-info" data-id="<?php echo $productid;?>">
                Add to Wish List
                <span class="glyphicon glyphicon-star"></span>
              </button>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </body>
</html>