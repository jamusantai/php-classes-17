<?php

session_start();
//check if user is logged in
if(!$_SESSION["account_id"]){
  //redirect to login page if user is not logged in
  header("location:login.php");
  exit();
}

//include database
include("includes/database.php");
//include functions
include("includes/functions.php");

//------get user data when page loads
$account_id = $_SESSION["account_id"];
//update user data when form is submitted
if($_SERVER["REQUEST_METHOD"]=="POST"){
  //store validation errors in array
  $errors = array();
  
  //------------ACCOUNT DATA----------------------------------------
  //get email from update and sanitize
  //-needs to be valid
  $update_email = filter_var($_POST["email"],FILTER_SANITIZE_EMAIL);
  //----validate email
  if(!filter_var($update_email,FILTER_VALIDATE_EMAIL)){
    $errors["email"] = "invalid email address";
  }
  
  //get username from update and sanitize
  $update_user = filter_var($_POST["username"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
  //----validate username
  
  //check length
  $update_user_length = strlen($update_user);
  if($update_user_length<6 || $update_user_length > 16){
    $errors["username"] = "username needs to be between 6-16 characters";
  }
  
  //check if alphanumeric
  $nospace = str_replace(" ","",$update_user);
  if(!ctype_alnum($nospace)){
    $message = "only A-Z,a-z,0-9 allowed";
    if($errors["username"]){
      $errors["username"]=$errors["username"]." and ".$message;
    }
    else{
      $errors["username"]=$message;
    }
  }
  
  //if no errors update username then email and capture any database errors (eg duplicate error)
  //updates are performed separately to capture duplicate errors for each
  if(count($errors)==0){
    $sql_errors = array();
    $sql_updates = array("username"=>$update_user,"email"=>$update_email);
    //run the query in a loop to catch all the errors
    foreach($sql_updates as $key => $value){
      $account_query = "UPDATE accounts SET $key='$value',updated=NOW() WHERE account_id='$account_id'";
      //run the query and log errors
      if(!$connection->query($account_query)){
        // $error_code = $connection->errno;
        $errors[$key] = "$key already in use";
      }
    }
  }
  
  //--check passwords
  //see if passwords have been submitted
  $password1 = $_POST["password1"];
  $password2 = $_POST["password2"];
  //if passwords are not empty
  if($password1 || $password2){
    $check = passwordCheck($password1,$password2);
    if($check["status"]!==true){
      $errors["password"] = $check["errors"];
    }
    else{
      //if there are no password errors, update the password
      $update_password = password_hash($password1,PASSWORD_DEFAULT);
      $pwquery = "UPDATE accounts 
      SET password='$update_password',
      updated=NOW()
      WHERE account_id='$account_id'";
      if(!$connection->query($pwquery)){
        $errors["update"] = "update error";
      }
    }
  }
  
  //------PROFILE IMAGE------------------------
  //only run if there is an file uploaded
  if($_FILES["profile"]["tmp_name"]){
    $image_errors = array();
    $uploaded_file = $_FILES["profile"]["tmp_name"];
    
    $uploaded_file_name = $_FILES["profile"]["name"];
    //check if the image is an image file by trying to read exif data
    
    
    //get the name of file uploaded
    $uploaded_name = pathinfo($uploaded_file_name,PATHINFO_FILENAME);
    
   
    //get the file type to check if it's an image
    $fileinfo = finfo_open();
    $uploaded_type = finfo_file($fileinfo, $uploaded_file, FILEINFO_MIME);
    finfo_close($fileinfo);
    
    //image checks http://php.net/manual/en/function.exif-imagetype.php
    $img_type = exif_imagetype($uploaded_file);
    //image checks http://php.net/manual/en/function.exif-imagetype.php
    if($img_type>3){
      array_push($image_errors,"only jpg,png or gif allowed");
    }
    
    //prevent dot files from being used, eg .htaccess
    //.htaccess is a server configuration file for apache and can be used to 
    //change the behaviour of the server
    if(strpos($uploaded_name,".",0)===0 || $uploaded_name == ".htaccess"){
      array_push($image_errors,"illegal character in file name");
    }
    
    //get the extension of the file uploaded
    $uploaded_extension = strtolower(pathinfo($uploaded_file_name,PATHINFO_EXTENSION));
    //check if file extension is either png,jpg or gif
    if($uploaded_extension!=="png" && $uploaded_extension!=="jpg"
    && $uploaded_extension!=="gif" && $uploaded_extension!=="jpeg"){
      array_push($image_errors,"extension not allowed");
    }
    
    //check filesize
    //get the file size of the file
    $uploaded_file_size = filesize($uploaded_file);
    if($uploaded_file_size>1048576){
      array_push($image_errors,"file should be smaller than 1MB");
    }
    
    //check image dimensions
    //get the file dimensions
    $uploaded_file_dims = getimagesize($uploaded_file);
    //if getting dimesions fail, it means it may not be an image
    if(!$uploaded_file_dims){
      array_push($image_errors,"file may not be an image");
    }
    //check if there are errors
    if(count($image_errors)>0){
      // output errors
      $errors["image_type"] = implode($image_errors," and ");
    }
    else{
      //get image data
      $type = $uploaded_file_dims["mime"];
      $width = $uploaded_file_dims[0];
      $height = $uploaded_file_dims[1];
      
      //rename image
      $newfile = uniqid("image",true).".".$uploaded_extension;
      //move image to profiles directory
      move_uploaded_file($_FILES["profile"]["tmp_name"],"profiles/".$newfile);
      //insert image into database
      $imgquery = "UPDATE accounts 
      SET profile_image='$newfile',
      updated=NOW() 
      WHERE account_id='$account_id'";
      if(!$connection->query($imgquery)){
        $errors["update"] = "error updating image";
      }
    }
  }
  
  //------USER PROFILE-------------------------
  //sanitize data from user profile data
  $firstname = filter_var($_POST["firstname"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
  $lastname = filter_var($_POST["lastname"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
  $streetnumber = filter_var($_POST["streetnumber"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
  $unitnumber = filter_var($_POST["unitnumber"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
  $streetname = filter_var($_POST["streetname"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
  $locality = filter_var($_POST["locality"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
  $state = filter_var($_POST["state"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
  $postcode = filter_var($_POST["postcode"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
  $country = filter_var($_POST["country"],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);

  $update_profile_query ="INSERT INTO users
  (account_id, first_name, last_name, street_no, unit, street_name, locality, state, postcode, country)
  VALUES
  ('$account_id', '$firstname', '$lastname', '$streetnumber', '$unitnumber', '$streetname', '$locality', '$state', '$postcode', '$country')
  ON DUPLICATE KEY UPDATE
  account_id='$account_id',first_name='$firstname',last_name='$lastname',street_no='$streetnumber',
  unit='$unitnumber',street_name='$streetname',locality='$locality',state='$state',postcode='$postcode',
  country='$country'";

  if(!$connection->query($update_profile_query)){
    $errors["update"] = "update error";
  }
}


function getUserData($conn,$id){
  $account_query = "SELECT username,email,profile_image FROM accounts WHERE account_id='$id'";
  $account_result = $conn->query($account_query);
  //we only expect one result, so no need for loop
  if($account_result->num_rows > 0){
    $account_data = $account_result->fetch_assoc();
    return $account_data;
  }
  else{
    return false;
  }
}

$user_data = getUserData($connection,$account_id);
if($user_data){
  $username = $user_data["username"];
  $useremail = $user_data["email"];
  $userprofile = $user_data["profile_image"];
  //update session data with fresh data from database
  $_SESSION["user_name"] = $user_data["username"];
  $_SESSION["user_email"] = $user_data["email"];
  $_SESSION["profile_image"] = $user_data["profile_image"];
}

$user_profile_query = "SELECT * FROM users WHERE account_id='$account_id'";
$user_profile_result = $connection->query($user_profile_query);
if($user_profile_result->num_rows > 0){
  $profile_data = $user_profile_result->fetch_assoc();

  $firstname = $profile_data["first_name"];
  $lastname = $profile_data["last_name"];
  $streetnumber = $profile_data["street_no"];
  $unitnumber = $profile_data["unit"];
  $streetname = $profile_data["street_name"];
  $locality = $profile_data["locality"];
  $state = $profile_data["state"];
  $postcode = $profile_data["postcode"];
  $country = $profile_data["country"];
}

//set the page title using the username
$page_title = "Account of $username";
?>

<!doctype html>
<html>
  <?php include("includes/head.php"); ?>  
  <body>
    <?php include("includes/navigation.php"); ?>
    <main class="container-fluid">
      <div class="row">
        <div class="col-md-4 col-offset-md-4 text-center">
          <?php 
          if($errors["update"]){
            $updatemsg = $errors["update"];
          }
          ?>
          <?php echo $updatemsg; ?>
        </div>
      </div>
      <form id="account-form" method="post" action="account.php" enctype="multipart/form-data">
        <div class="row">
          <div class="col-md-2">
            <h3>Profile Image</h3>
            <div class="profile-container round">
              <?php 
              if($_SESSION["profile_image"]){
                $profile_img = $_SESSION["profile_image"];
              }
              else{
                $profile_img = "default.jpg";
              }
              ?>
              <img class="profile img-responsive" 
              id="profile-image" 
              src="profiles/<?php echo $profile_img; ?>">
            </div>
            <?php 
            if($errors["email"]){
              $image_class="has-error";
              $image_message=$errors["image-type"];
            }
            ?>
            <div class="form-group <?php echo $image_class; ?>" id="image-upload">
              <label for="profile-input" class="btn btn-default btn-block">
                <input type="file" id="profile-input" name="profile" accept="image/*">
                <span class="glyphicon glyphicon-pencil"></span>
               <span id="selected-image">Select image</span>
              </label>
              <span id="profile-error" class="help-block">
                <?php echo $image_message; ?>
              </span>
            </div>
          </div>
          <!--Account Details-->
          <div class="col-md-4">
            <h3>Account Details</h3>
            <?php
            if($errors["email"]){
              $email_class="has-error";
              $email_message=$errors["email"];
            }
            if($update_email){
              $useremail = $update_email;
            }
            ?>
            <div class="form-group <?php echo $email_class; ?>">
              <label for="email">Email</label>
              <input type="email" required name="email" id="email" class="form-control" 
              value="<?php echo $useremail; ?>">
              <span class="help-block"><?php echo $email_message; ?></span>
            </div>
            <?php
            if($errors["username"]){
              $username_class="has-error";
              $username_message=$errors["username"];
            }
            if($update_user){
              $username = $update_user;
            }
            ?>
            <div class="form-group <?php echo $username_class; ?>">
              <label for="username">Username</label>
              <input type="text" required name="username" id="username" class="form-control"
              value="<?php echo $username; ?>">
              <span class="help-block"><?php echo $username_message; ?></span>
            </div>
            <!--password block-->
            <?php
            if($errors["password"]){
              $password_class="has-error";
              $password_message=$errors["password"];
              $password_open = "in";
            }
            ?>
            <button id="change-password" class="btn btn-warning" type="button" data-toggle="collapse" data-target="#password-collapse">
              Change Password
            </button>
            <div id="password-collapse" class="collapse <?php echo $password_open;?>">
              <div id="password-group" class="form-group <?php echo $password_class; ?>">
                <label for="password1">New Password</label>
                <input type="password" name="password1" id="password1" autocomplete="off" class="form-control">
                <label for="password2">Retype New Password</label>
                <input type="password" name="password2" id="password2" autocomplete="off" class="form-control">
                <span id="password-error" class="help-block"><?php echo $password_message; ?></span>
              </div>
            </div>
            <!--end of password block-->
          </div>
          <div class="col-md-6">
            <h3>Profile Details</h3>
            <div class="form-group">
              <label for="firstname">First Name</label>
              <input type="text" name="firstname" id="firstname" class="form-control"
              value="<?php echo $firstname; ?>">
              <span class="help-block"></span>
            </div>
            <div class="form-group">
              <label for="lastname">Last Name</label>
              <input type="text" name="lastname" id="lastname" class="form-control"
              value="<?php echo $lastname; ?>">
              <span class="help-block"></span>
            </div>
            <!--street and unit number-->
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="streetnumber">Street Number</label>
                  <input type="text" name="streetnumber" id="streetnumber" class="form-control"
                  value="<?php echo $streetnumber; ?>">
                  <span class="help-block"></span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="unitnumber">Unit Number</label>
                  <input type="text" name="unitnumber" id="unitnumber" class="form-control"
                  value="<?php echo $unitnumber; ?>">
                  <span class="help-block"></span>
                </div>
              </div>
            </div>
            <!--end of street and unit number-->
            <div class="form-group">
              <label for="streetname">Street Name</label>
              <input type="text" name="streetname" id="streetname" class="form-control"
              value="<?php echo $streetname; ?>">
              <span class="help-block"></span>
            </div>
            <!--Locality-->
            <div class="form-group">
              <label for="locality">Locality</label>
              <input type="text" name="locality" id="locality" class="form-control"
              value="<?php echo $locality; ?>">
              <span class="help-block"></span>
            </div>
            <!--state and postcode-->
            <?php
            $states_query = "SELECT sub_region_code,sub_region_name FROM 
            countries_subdivisions WHERE country_code='$country'";
            $states_result = $connection->query($states_query);
            ?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="state">State</label>
                  <select class="form-control" id="state" name="state">
                    <option value=""></option>
                    <?php 
                    if($states_result->num_rows > 0){
                      while($row = $states_result->fetch_assoc()){
                        $statecode = $row["sub_region_code"];
                        $statename = $row["sub_region_name"];
                        if($statecode == $state){
                          $selectedstate = "selected";
                        }
                        else{
                          $selectedstate = "";
                        }
                        echo "<option $selectedstate value=\"$statecode\">$statename</option>";
                      }
                    }
                    ?>
                  </select>
                  <span class="help-block"></span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="postcode">Postcode</label>
                  <input type="text" name="postcode" id="postcode" class="form-control"
                  value="<?php echo $postcode; ?>">
                  <span class="help-block"></span>
                </div>
              </div>
            </div>
            <!--end of state and postcode-->
            <?php
            //get list of countries from database
            $countries_query = "SELECT country_id,country_name FROM countries";
            $countries_result = $connection->query($countries_query);
            ?>
            <div class="form-group">
              <label for="country">Country</label>
              <select name="country" id="country" name="country" class="form-control">
                <option value=""></option>
                <?php
                if($countries_result -> num_rows > 0){
                  while($row = $countries_result->fetch_assoc()){
                    $listcountry = $row["country_name"];
                    $listcountryid = $row["country_id"];
                    if($listcountryid==$country){
                      $selectedcountry = "selected";
                    }
                    else{
                      $selectedcountry = "";
                    }
                    echo "<option $selectedcountry value=\"$listcountryid\">$listcountry</option>";
                  }
                }
                ?>
              </select>
              <span class="help-block"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2 col-md-offset-5">
            <button type="submit" id="submit" name="submit" value="submit" class="btn btn-info btn-block">
              Save Changes
            </button>
          </div>
        </div>
      </form>
    </main>
  </body>
</html>