<?php 
print_r($_GET);
//process get variable
$getcategory = filter_var($_GET["category"],FILTER_SANITIZE_NUMBER_INT);

//move database to "includes as a snippet"
include("includes/database.php");
//modify query to allow for INNER JOIN
$query = "SELECT products.id,products.name,products.price FROM products";
//if there is an id set
if(isset($getcategory) && $getcategory!=NULL){
  $query = $query." INNER JOIN products_categories 
  ON products.id=products_categories.product_id 
  WHERE products_categories.category_id='$getcategory'";
}

$result = $connection->query($query);
$products = array();
if($result->num_rows > 0){
  while($row = $result->fetch_assoc())
  {
    array_push($products,$row);
  }  
}
//categories
$catquery="SELECT category_id,category_name FROM categories WHERE active=1";
$catresult = $connection->query($catquery);
//store in array
$categories = array();
if($catresult->num_rows > 0){
  while($catrow = $catresult->fetch_assoc())
  {
    array_push($categories,$catrow);
  }
}

?>
<!doctype html>
<html>
    <!--move head to includes and add bootstrap css-->
    <?php include("includes/head.php");?>
    <body>
      <div class="container">
        <h1>Welcome to Home!</h1>
        <form id="search-form" method="get" action="search.php">
            <input type="text" placeholder="search words" name="query">
            <button type="submit" name="submit">Search</button>
        </form>
      </div>
        
      <div class="container">
        <!--divide into two columns, for categories sidebar-->
        <div class="row">
          <aside class="col-md-2">
            <h4>Categories</h4>
            <form id="form-categories" action="index.php" method="get">
            <?php
            foreach($categories as $cat_item)
            {
              $catname = $cat_item["category_name"];
              $catid = $cat_item["category_id"];
              echo "<div class=\"checkbox\">
                    <label>
                      <input type=\"checkbox\" name=\"categories[]\" value=\"$catid\">
                      $catname
                    </label>
                    </div>";
            }
            ?>
            <button class="btn btn-default" type="submit" value="submit" name="submit">
              Filter
            </button>
            </form>
            <h4>Categories</h4>
            <ul class="nav nav-pills nav-stacked">
            <!--output categories-->
            <?php
            //create link for all cagetories view
            if(!isset($getcategory) || $getcategory == NULL){
              echo "<li class=\"active\">";
            }
            else{
              echo "<li>";
            }
            echo "<a href=\"index.php\">All</a>";
            echo "</li>";
            //output categories from database
            foreach($categories as $cat_item){
              $cat_name = $cat_item["category_name"];
              $cat_id = $cat_item["category_id"];
              //set active to indicate current category
              if($getcategory == $cat_id){
                echo "<li class=\"active\">";
              }
              else{
                echo "<li>";
              }
              echo "<a href=\"index.php?category=$cat_id\">$cat_name</a>";
              echo "</li>";
            }
            ?>
            </ul>
          </aside>
          <main class="col-md-10">
          <?php
          $productnumbers = count($products);
          $counter = 0;
          for($i=0;$i<$productnumbers;$i++)
          {
            $productname = $products[$i]["name"];
            $productid = $products[$i]["id"];
            $productprice = $products[$i]["price"];
            
            $counter++;
            if($counter===1){
              echo "<div class=\"row\">";
            }
            echo "<div class=\"col-md-3 col-sm-6 home-products\">";
             echo "<h3>$productname</h3>";
             
            echo "<p class=\"price\">$productprice</p>";
            echo "<a href=\"view.php?id=$productid\">View Detail</a>";
            echo "</div>";
            if($counter >=4 || $counter==$productnumbers){
              echo "</div>";
              $counter = 0;
            }
          }
          ?>
          </main>
        </div>
      </div>
    </body>
</html>