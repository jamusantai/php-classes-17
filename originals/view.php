<?php
//move database to includes/database.php
include("includes/database.php");
//make sure that get variable is sanitized before adding to query
$productid = filter_var($_GET["id"],FILTER_SANITIZE_NUMBER_INT);

if($productid){
  $query = "SELECT name,description,price FROM products WHERE id='$productid'";
  $result = $connection->query($query);
  $product = array();
  if($result->num_rows > 0){
      $row = $result->fetch_assoc();
      array_push($product,$row);
  }
}
?>

<!doctype html>
<html>
    <!--move head section to includes/head.php-->
    <?php include("includes/head.php"); ?>
    <body>
      <!--add bootstrap markup-->
      <div class="container">
        <div class="row">
          <div class="col-md-4">
          <?php
          foreach($product as $item){
              $name = $item["name"];
              $description = $item["description"];
              $price = $item["price"];
              echo "<h3>$name</h3>";
              echo "<p>$description</p>";
              echo "<p>$price</p>";
          }
          ?>
          </div>
        </div>
        
      </div>
    </body>
</html>