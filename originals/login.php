<?php
include("includes/database.php");
session_start();
//check if a form is being submitted
if($_SERVER["REQUEST_METHOD"]=="POST"){
  $useremail = $_POST["useremail"];
  $password = $_POST["password"];
  if(filter_var($useremail,FILTER_VALIDATE_EMAIL)){
    //user submitted an email address
    $query = "SELECT account_id,email,username,password FROM accounts WHERE email='$useremail'";
  }
  else{
    //user submitted a username
    //sanitize the value before adding to query
    $username = filter_var($useremail,FILTER_SANITIZE_STRING,FILTER_SANITIZE_STRIP_HIGH);
    $query = "SELECT account_id,email,username,password FROM accounts WHERE username='$username'";
  }
  $user_result = $connection->query($query);
  if($user_result -> num_rows > 0){
    $userdata = $user_result->fetch_assoc();
    $id = $userdata["account_id"];
    $email = $userdata["email"];
    $hashed = $userdata["password"];
    $username = $userdata["username"];
  }
  
  if(!password_verify($password,$hashed)){
    //create error
    $error = "credentials supplied do not match our records";
  }
  else{
    //password matches
    //regenerate session id
    session_regenerate_id();
    //update login time
    $account_id = $userdata["account_id"];
    $login_query = "UPDATE accounts SET lastlogin=NOW() WHERE account_id='$account_id'";
    $updatelogintime = $connection->query($login_query);
    //create session variables
    $_SESSION["account_id"] = $userdata["account_id"];
    $_SESSION["user_email"] = $userdata["email"];
    $_SESSION["user_name"]= $userdata["username"];
    //redirect to account.php
    header("location:account.php");
  }
}

?>
<!doctype html>
<html>
  <?php include("includes/head.php"); ?>
  <body>
    <?php include("includes/navigation.php");?>    
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <form id="login-form" method="post" action="login.php">
            <h2>Log in</h2>
            <?php
            if($error){
              $login_error_class = "has-error";
              $login_message = $error;
            }
            ?>
            <div class="form-group <?php echo $login_error_class; ?>">
              <label for="useremail">Email or Username</label>
              <input type="text" name="useremail" id="useremail" required placeholder="username or you@domain.com" class="form-control">
            </div>
            <div class="form-group <?php echo $login_error_class; ?>">
              <label for="password">Password</label>
              <input type="password" name="password" id="password" required placeholder="password" class="form-control">
              <span class="help-block"><?php echo $login_message; ?></span>
            </div>
            <div class="text-center">
              <button type="submit" name="login" id="login" class="btn btn-info">
                Sign In
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>