<?php
session_start();
include("includes/functions.php");
//capture any get variables
if(count($_GET["categories"]) > 0){
  //get selected categories
  $selected_cats = filter_var_array($_GET["categories"],FILTER_SANITIZE_NUMBER_INT);
}

//capture selected maximum price
if($_GET["price"]){
  $selected_price = filter_var($_GET["price"],FILTER_SANITIZE_NUMBER_INT);
}
include("includes/database.php");
//connect to get categories from database
$cat_query = "SELECT category_id,category_name FROM categories WHERE active=1";
$cat_result = $connection->query($cat_query);
$categories = array();
if($cat_result->num_rows > 0){
  while($cat_row = $cat_result->fetch_assoc()){
    array_push($categories,$cat_row);
  }
}

//get products from the database
$products_query = "SELECT products.id,products.name,products.price,images.image_file
FROM products 
LEFT JOIN products_images 
ON products_images.product_id = products.id
LEFT JOIN images ON products_images.image_id = images.image_id";

//add category filters
$category_filter = " "."INNER JOIN products_categories
ON products.id=products_categories.product_id WHERE"." ";
//count selected categories
$cat_count = count($_GET["categories"]);

if($cat_count > 0){
  for($i=0;$i<$cat_count;$i++){
    $cat_id = $_GET["categories"][$i];
    if($i==0){
      $category_filter = $category_filter." ".
      "(products_categories.category_id='$cat_id'";
    }
    else{
      $category_filter = $category_filter." ".
      "OR products_categories.category_id='$cat_id'";
    }
    if($i==$cat_count-1){
      $category_filter = $category_filter.") ";
    }
  }
  $products_query = $products_query.$category_filter;
}
//filter by price
if($selected_price){
  if($cat_count > 0){
    $price_filter = " "."AND products.price <= '$selected_price'";
  }
  else{
    $price_filter = " "."WHERE products.price <= '$selected_price'";
  }
  $products_query = $products_query.$price_filter;
}
//set grouping of product results
$products_query = $products_query." "."GROUP BY products.id";


//-----PAGINATION----------------------
//receive the page GET request or if none, set to 1
if($_GET["page"]){
  //we expect page number to be an integer, so we sanitise it as an integer
  $page = filter_var($_GET["page"],FILTER_SANITIZE_NUMBER_INT);
}
else{
  $page = 1;
}
//run the query to get total of results
$products_total_result = $connection->query($products_query);
//store the total number of results
$totalrecords = $products_total_result->num_rows;

//set items per page
$itemsperpage = 12;

//get total number of pages using ceil, eg if last page
//has less than 12 items it still counts as a page
$totalpages = ceil($totalrecords/$itemsperpage);

$offset = ($page-1)*$itemsperpage;

//modify the query and add LIMIT and OFFSET for pagination of results
$products_query = $products_query." "."ORDER BY products.id ASC LIMIT $itemsperpage OFFSET $offset";

//-----query the database for products result
$products_result = $connection->query($products_query);

$products = array();
if($products_result->num_rows > 0){
  while($products_row = $products_result->fetch_assoc()){
    array_push($products,$products_row);
  }
}

?>

<!doctype html>
<html>
  <?php include("includes/head.php"); ?>
  <body>
    <?php include("includes/navigation.php"); ?>
    <div class="container">
      <div class="row">
        
      </div>
      <div class="row">
        <!--left sidebar-->
        <aside class="col-md-2 sidebar">
          <?php
          include("includes/product_filters.php");
          ?>
        </aside>
        <!--content area-->
        <div class="col-md-10">
          <!--pagination include-->
          <?php include("includes/pagination.php"); ?>
          <!--product rows-->
          <?php
             // product loop to generate rows and columns
            $productcount = count($products);
            $products_per_row = 4;
            $col_width = 12/$products_per_row;
            $counter=0;

            for($i=0;$i<$productcount;$i++){
              $counter++;
              // everytime the counter is equal 1 create a new row
              if($counter==1){
                echo "<div class=\"row product-row\">";
              }
              $id = $products[$i]["id"];
              $name = $products[$i]["name"];
              $price = $products[$i]["price"];
              $image = $products[$i]["image_file"];
              echo "<div class=\"col-md-$col_width col-xs-6 front-product\">
              <h4 class=\"product-name\">$name</h4>
              <img class=\"img-responsive\" src=\"images/$image\">
              <div class=\"product-toolbar\">
                <span class=\"price front\">$price</span>
                <a class=\"btn btn-default btn-sm\" href=\"view.php?id=$id\">Detail</a>
              </div>
              </div>";
              if($counter==$products_per_row || $i==$productcount-1){
                echo "</div>";
                $counter = 0;
              }
            }
          ?>
          
          <?php include("includes/pagination.php"); ?>
        </div>
    </div>
    </div>
  </body>
</html>