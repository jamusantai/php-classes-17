<?php
include("includes/functions.php");
//capture GET variable called "token"
if(!$_GET["token"]){
  exit();
}

//array to contain errors
$errors = array();

if($_SERVER["REQUEST_METHOD"]=="GET" && $_GET["token"]){
  //get token value from GET request in email link
  $token = filter_var($_GET["token"],FILTER_SANITIZE_STRING);
  //check token with database
  $token_query = "SELECT account_id,token,created FROM password_reset WHERE token='$token' AND active=1";

  include("includes/database.php");
  $token_result = $connection->query($token_query);
  if($token_result->num_rows > 0){
    $reset_row = $token_result->fetch_assoc();
    //token from database
    $stored_token = $reset_row["token"];
    //date when token created
    $created = $reset_row["created"];
    //users account_id
    $account_id = $reset_row["account_id"];
    
    
    //--check if token is still valid
    //--maximum life is 2 hours
    $token_max_life = 2;
    //time right now in timestamp
    $now = strtotime(date("Y-m-d H:i:s"));
    //time when token crated in timestamp
    $token_creation = strtotime(date($created));
    //the amount of time that has lapsed $lapsed=(seconds/3600)
    $lapsed = ($now-$token_creation)/3600;
    if($lapsed > 2){
      //token has expired
      $errors["expired"] = "Sorry, the link you were sent has expired.";
    }
  }
  else{
    $errors["invalid"] = "Sorry, The link is invalid";
  }
}

//if password is being reset and new password set
if($_SERVER["REQUEST_METHOD"]=="POST"){
  $password1 = $_POST["password1"];
  $password2 = $_POST["password2"];
  //check for password errors
  $password_check = checkPasswords($password1,$password2);
  if($password_check!==true){
    
  }
}
$page_title = "Create a new password";
?>
<!doctype html>
<html>
  <?php include("includes/head.php"); ?>
  <body>
    <?php include("includes/navigation.php");?>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <?php
          if(count($errors)>0){
            $message = implode($errors," and ");
            echo "<div class=\"alert alert-danger\">
                <p>$message</p>
              <a class=\"btn btn-danger\" href=\"password-reset.php\">Reset password again</a>
            </div>";
            exit();
          }
          else{
            //print form to reset password
            $message = "Please type your new password";
            $this_page = basename($_SERVER["PHP_SELF"]);
            echo "<form id=\"\" action=\"$this_page\" method=\"post\">";
            echo "<div class=\"form-group\">";
            echo "<label for=\"password1\">Type your new password</label>";
            echo "<input name=\"password1\" type=\"password\" id=\"password1\" class=\"form-control\">";
            echo "<label for=\"password2\">Retype your new password</label>";
            echo "<input name=\"password2\" type=\"password\" id=\"password2\" class=\"form-control\">";
            echo "</div>";
            echo "<div class=\"text-center\">
            <button class=\"btn btn-info\">Reset Password</button>
            </div>";
            echo "</form>";
          }
          ?>
        </div>
      </div>
    </div>
  </body>
</html>