<?php
include("includes/database.php");
//set to false for debugging
$supress_errors = true;
//receive post data
if($_SERVER["REQUEST_METHOD"]=="POST"){
  $reset_email = filter_var($_POST["reset-email"],FILTER_SANITIZE_EMAIL);
  //create errors array
  $errors=array();
  //validate the email
  if(!filter_var($reset_email,FILTER_VALIDATE_EMAIL)){
    $errors["validity"] = "not a valid email address";
  }
  //check if there is no error so far
  if(count($errors)==0){
    //check if email exists in the database
    //create query
    $reset_query = "SELECT account_id,email FROM accounts WHERE email='$reset_email'";
    $result = $connection->query($reset_query);
    if($result->num_rows == 0){
      $errors["account"] = "account does not exist";
    }
    else{
      //account id and email
      $userdata = $result->fetch_assoc();
      $account_id = $userdata["account_id"];
      $account_email = $userdata["email"];
      //proceed to reset password
      //generate a random token
      $token = bin2hex(openssl_random_pseudo_bytes(16));
      //insert into password_reset table and log the time in created
      $pw_reset_query = "INSERT INTO password_reset 
      (account_id,token,created,active) VALUES('$account_id','$token',NOW(),1) ON DUPLICATE KEY UPDATE account_id='$account_id',token='$token',created=NOW(),active=1";
      //check if insert is successful
      if(!$connection->query($pw_reset_query)){
        $errors["database"] = "database error";
      }
      else{
        // generate email
        $server_address = "http://".$_SERVER["SERVER_NAME"]."/";
        $reset_link = $server_address."change-password.php"."?token=$token";
        $reset_message = "You or someone has requested a password reset for your account at. Click on this link
        to reset your password \r\n
        <a href=\"$reset_link\">$reset_link</a>";
        $headers = "MIME-Version: 1.0". "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "johannes.edu@gmail.com" . "\r\n";
        
        $subject = "Password Reset Request";
        if(!mail($account_email,$subject,$reset_message,$headers)){
          $errors["send"] = "error sending email";
        }
        else{
          $success = "An email has been sent to your registered address. Please check your email";
        }
      }
    }
  }
}
$page_title = "Reset your password";


?>
<!doctype html>
<html>
<?php include("includes/head.php");?>
<body>
  <?php include("includes/navigation.php");?>
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <span class="help-block">
        <?php
        if($success){
          echo $success;
        }
        elseif(count($errors)>0 && $supress_errors==false){
          echo implode($errors," and ");
        }
        ?>
        </span>
        <h3>Reset Password</h3>
        <p>Please type your email here and we will send you an email to 
        help you reset your password. </p>
        <form id="reset-form" action="password-reset.php" method="post">
          <?php
          if(($errors["validity"] || $errors["exists"]) && $supress_errors==false){
            $error_class = "has-error";
            $combined_error = implode(" ",$errors);
          }
          ?>
          <div class="form-group <?php echo $error_class;?>">
            <label for="reset-email">Your Email Address</label>
            <input type="email" 
            name="reset-email" 
            id="reset-email" 
            class="form-control"
            placeholder="you@domain.com">
            <span class="help-block"><?php echo $combined_error; ?></span>
          </div>
          <div class="text-center">
            <button class="btn btn-warning" type="submit" name="submit">Reset Your Password</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</body>
</html>