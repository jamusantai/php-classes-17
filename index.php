<?php
include("autoloader.php");

$categories = new Categories();
$page_title="Home Page";

?>
<!doctype html>
<html>
  <?php include("includes/head.php");?>
  <body>
    <?php include_once("includes/pagenavigation.php"); ?>
    <div class="container">
      <div class="row">
        <!--category selector-->
        <div class="col-md-2">
          <h4>Filter by categories</h4>
          <form id="categories" action="index.php" method="get">
            <?php $categories->render(); ?>
            <button class="btn btn-default" type="reset" name="reset">
              Reset
            </button>
          </form>
        </div>
        <div class="col-md-10">
          <div class="row">
            <div class="col-md-6">
              <form id="search-form" class="form-inline" action="index.php" method="get">
                <div class="form-group">
                  <label class="sr-only" for="search">Search Terms</label>
                  <input id="search" class="form-control" placeholder="search" type="text" name="search">
                </div>
                <button class="btn btn-info">
                  <i class="glyphicon glyphicon-search"></i>
                </button>
              </form>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <p class="totalresult"></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" id="products-container">
              
            </div>
          </div>
           <?php //$products->renderProducts(2); ?>
        </div>
      </div>
      <!--<div class="row">-->
      <!--  <?php //echo $products->getMaxPrice(); ?>-->
      <!--</div>-->
    </div>
    <script src="js/products.js"></script>
    <script src="js/categories.js"></script>
    
    <!--template to display products-->
    <template id="product">
      <div class="products col-md-3 col-sm-6">
        <h3 class="product-title product-name"><!--Product Name--></h3>
        <img class="product-image img-responsive">
        <p class="price"></p>
        <p class="product-desc"></p>
        <a class="product-detail" href="">Details</a>
      </div>
    </template>
    
    <!--template for a product row-->
    <template id="row">
      <div class="row">
      </div>
    </template>
  </body>
</html>