<?php
//autoloader for within ajax folder (resolve path to classes folder)
// include("ajaxautoloader.php");
include_once("../autoloader.php");
if(isset($_POST["identity"])){
  $data = array();
  
  //initialise an account object
  $account = new Account();
  $auth = $account->authenticate($_POST["identity"],$_POST["password"]);
  //$auth = $account->authenticate("test@gmail.com","password");
  if($auth){
    $data["success"] = true;
    $data["email"] = $_SESSION["email"];
    $data["username"] = $_SESSION["username"];
  }
  else{
    $data["success"] = false;
  }
  echo json_encode($data);
}

?>