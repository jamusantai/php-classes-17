<?php

include_once("../autoloader.php");
if(isset($_POST["register"])){
  $data = array();
  $errors = array();
  //receive POST data
  //sanitize email address
  $email = filter_var($_POST["email"],FILTER_SANITIZE_EMAIL);
  $username = filter_var($_POST["username"],FILTER_SANITIZE_STRING);
  //check password
  $password = $_POST["password"];
  //if password is too short
  if(strlen($password) < 8){
    $errors["password_length"] = "minimum 8 characters";
  }
  //initialise an account object
  $account = new Account();
  $auth = $account->create($email,$password,$username);
  if($auth){
    $data["success"] = true;
    $data["email"] = $_SESSION["email"];
    $data["username"] = $_SESSION["username"];
  }
  else{
    $data["success"] = false;
  }
  echo json_encode($data);
}

?>